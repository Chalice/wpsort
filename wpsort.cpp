/*
        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
                    Version 2, December 2004 

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net> 

 Everyone is permitted to copy and distribute verbatim or modified 
 copies of this license document, and changing it is allowed as long 
 as the name is changed. 

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 

  0. You just DO WHAT THE FUCK YOU WANT TO.
*/

/*
  WPSort 0.2.1
  ------------
  This Linux commandline program takes a source folder, recursively
  searches it for images, and copies the images into a (autocreated)
  destination folder if the found image falls within a minimum and
  maximum image ratio range, and has over a specified amount of
  minimum total pixels.

  Why? Grabbing all potential images usable as a wallpaper!

  Inspired by the free Windows program Dimensions2Folders. I just
  couldn't find a damn Linux equivalent.
  C++, Totally written in C style. Lazyness is everything.

  System requirements:
  Merely system libraries.

  Compile:
  g++ -O3 wpsort.cpp -o wpsort

  Usage:
  ./wpsort <src> <dest> <minratio> <maxratio> <minpixels>

  src - Source folder
  dest - Destination folder
  minratio - Minimum ratio (ex. 1.4)
  maxratio - Maximum ratio (ex. 2.2)
  minpixels - Minimum image pixels (ex. 700000)
*/
#include <string>
#include <iostream>
#include <ftw.h> // Recursive directory searching
#include <fnmatch.h> // Filematching
#include <netinet/in.h>
using namespace std; // Bestspace

static const char *FILETYPES[] = {
  "*.bmp", "*.jpg", "*.jpeg", "*.png", "*.tga", "*.gif"
};

float fMinRatio; // Minimum Ratio
float fMaxRatio; // Maximum Ratio
int iMinPixels; // MIminmum amount of pixels in the images

struct stat st; // Stat struct for handling source/destination directories
std::string sDestPath; // Destination directory name.

// Per-file callback for recursive ftw() folder searching
static int fmatch_callback(const char *fpath, const struct stat *sb, int typeflag, struct FTW *ftwbuf)
{
  int iFileType=-1;
  if(typeflag == FTW_F) // It's a file
  {
    // Go ahead and loop through filetypes to check for
    for(int i = 0; i< (sizeof(FILETYPES) / sizeof(FILETYPES[0])); i++)
      if(fnmatch(FILETYPES[i], fpath, FNM_CASEFOLD) == 0) // Filetype matches
      {
        iFileType=i;
        break;
      }
    if(iFileType > -1)
    {
      FILE *inFile;
      inFile = fopen(fpath,"r"); // Open source file.
      if(inFile == NULL)
      {
        cout << "Cannot open file: " << fpath << endl;
        return 1;
      }
      int iWidth=-1; // Image width
      int iHeight=-1; // Image height
      int iSeek=0;
      bool bDone=false;
      unsigned char buf[8192];
      ssize_t data;
      unsigned char* cData;
      bool gotMarker=false;
      int jpeglol;
      switch(iFileType) // Get image width and height
      {
        case 0:
          // BMP
          data = fread(buf, 1, 26, inFile);
          if(data < 26)
          {
            iSeek = 999;
            cout << "Error reading BMP: " << fpath << endl;
            break;
          }
          iWidth = *(int*)&buf[18];
          iHeight = *(int*)&buf[22];
          break;
        case 1:
        case 2:
          // JPG
          while(1)
          {
            data = fread(buf, 1, sizeof(buf), inFile);
            if(data < 10) break;
            for(iSeek=0;iSeek < data;iSeek++)
            {
              cData=&buf[iSeek]; 
              jpeglol=(uint8_t)*cData;
              switch((uint8_t)*cData)
              {
                case 0xff:
                  gotMarker=true;
                  break;
                case 0xC0:
                case 0xC1:
                case 0xC2:
                case 0xC3:
                case 0xC5:
                case 0xC6:
                case 0xC7:
                case 0xC9:
                case 0xCA:
                case 0xCB:
                case 0xCD:
                case 0xCE:
                case 0xCF:
                  if(gotMarker == true)
                  {
                    if((data-iSeek) < 8)
                    {
                      fseek(inFile, -10, SEEK_CUR);
                      gotMarker=false;
                      break;
                    }
                    iHeight= (unsigned short) ((*(&buf[iSeek+4])<<8) + *&buf[iSeek+5]);
                    iWidth= (unsigned short) ((*(&buf[iSeek+6])<<8) + *&buf[iSeek+7]);
                    if(((iWidth + iHeight) > 512) && ((iWidth + iHeight) < 25000) )
                      bDone=true;
                    gotMarker=false;
                  }
                  break;
                default:
                  gotMarker=false;
                  break;
              }
            } 
            if(bDone == true)
              break;
          }
          iSeek=0;
          break;
        case 3:
          // PNG
          data = fread(buf, 1, 24, inFile);
          if(data < 24)
          {
            iSeek = 999;
            cout << "Error reading PNG: " << fpath << endl;
            break;
          }
          iWidth = *(int*)&buf[16];
          iHeight = *(int*)&buf[20];
          iWidth = ntohl(iWidth);
          iHeight = ntohl(iHeight);
          break;
        case 4:
          // TGA
          data = fread(buf, 1, 16, inFile);
          if(data < 16)
          {
            iSeek = 999;
            cout << "Error reading TGA: " << fpath << endl;
            break;
          }
          iWidth = *(short*)&buf[12];
          iHeight = *(short*)&buf[14];
          break;
        case 5:
          // GIF
          data = fread(buf, 1, 10, inFile);
          if(data < 10)
          {
            iSeek = 999;
            cout << "Error reading GIF: " << fpath << endl;
            break;
          }
          iWidth=*(short*)&buf[6];
          iHeight=*(short*)&buf[8];
          break;
        default:
          cout << "THIS IS NOT HAPPENING!" << endl;
          exit(1);
          break;
      }
      if(iWidth==-1)
      {
        cout << "NO dimensions: " << fpath << endl;
        return 1;
      }
      if(iSeek > 785) return 1; // We had an image parsing error
      fseek(inFile, 0, SEEK_SET); // seek Back to first file byte
      float fRatio=static_cast<float>(iWidth)/static_cast<float>(iHeight); // Ratio
      if(fRatio >= fMinRatio)
        if(fRatio <= fMaxRatio)
          if((iWidth*iHeight) >= iMinPixels)
          {
            // The file meets our preferences. Copy it.
            std::string sFinalFile=sDestPath;
            sFinalFile+="/";
            sFinalFile+=ftwbuf->base;
            FILE *outFile;
            umask(0660);
            outFile = fopen(sFinalFile.c_str(), "w");
            if(outFile == NULL)
            {
              cout << "Cannot open destination file: " << sFinalFile << endl;
              fclose(inFile);
            }
            else
            {
              // Copy binary-safe
              while(1)
              {
                data = fread(buf, 1, sizeof(buf), inFile);
                if(!data) break;
                if(!(fwrite(buf, 1, data, outFile) == data))
                {
                  cout << "Error writing file: " << sFinalFile << endl;
                  fclose(inFile);
                  fclose(outFile);
                  break;
                }
              }
              fclose(outFile);
              cout << sFinalFile << endl;
            }
          }
      fclose(inFile);
    }
  }
  return 0;
};

int main(int argc, char* argv[])
{
  if(argc == 6) // Got all necessary args
  {
    if(stat(argv[1],&st) == 0) // Can stat source folder
      if(S_ISDIR(st.st_mode)) // Is it a directory?
      {
        if(stat(argv[2],&st) == 0) // Does destination exist?
        {
          if(!S_ISDIR(st.st_mode)) // Is it a directory?
          {
            cout << "Destination is not a folder!" << endl;
            exit(1);
          }
        }
        else
        {
            if(mkdir(argv[2],S_IRWXU) == -1) // Create destination directory
            {
              cout << "Cannot create destination folder!" << endl;
              exit(1);
            }
        }
        // Store commandline args in according variables
        sDestPath=argv[2];
        fMinRatio=atof(argv[3]);
        fMaxRatio=atof(argv[4]);
        iMinPixels=atoi(argv[5]);
        nftw(argv[1], fmatch_callback, 16, FTW_PHYS | FTW_MOUNT); // Recusrively check source dir.
      }
      else
        cout << "Cannot find source folder!" << endl;
    else
      cout << "Cannot stat source folder!" << endl;
  }
  else
    cout << "Usage: " << argv[0] << " [frompath] [topath] [minratio] [maxratio] [minpixels]" << endl;
}

WPSort
  --------
  This Linux commandline program takes a source folder, recursively
  searches it for images, and copies the images into a (autocreated)
  destination folder if the found image falls within a minimum and
  maximum image ratio range, and has over a specified amount of
  minimum total pixels.

  Why? Grabbing all potential images usable as a wallpaper!

  Inspired by the free Windows program Dimensions2Folders. I just
  couldn't find a damn Linux equivalent.
  C++, Totally written in C style. Lazyness is everything.

  System requirements:
  Merely system libraries.

  Compile:
  g++ -O3 wpsort.cpp -o wpsort

  Usage:
  ./wpsort <src> <dest> <minratio> <maxratio> <minpixels>

  src - Source folder
  dest - Destination folder
  minratio - Minimum ratio (ex. 1.4)
  maxratio - Maximum ratio (ex. 2.2)
  minpixels - Minimum image pixels (ex. 700000)
